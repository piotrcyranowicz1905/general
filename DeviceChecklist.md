Device Checklist
================

This Checklist is a supplement for all porters to be able to inform users in detail about their progress, as well as track their readiness for being added as a community or core device.

For either qualifying as a community or a core device a corresponding feature set from the list below needs to be confirmed working. The details will be published soon here.

For the convenience of your users present this list in the following way:

Working
-------

Working with additional steps
-----------------------------

Not working
-----------

Put all working features in under the first headline. Put features where the user needs to to additonal (manual) steps under the second headline. Everything else goes under the third headline.

* Actors: Manual brightness
* Actors: Notification LED
* Actors: Torchlight
* Actors: Vibration
* Bluetooth: Driver loaded at startup
* Bluetooth: Enable/disable and flightmode works
* Bluetooth: Persistent MAC address between reboots
* Bluetooth: Pairing with headset works, volume control ok
* Camera: Flashlight
* Camera: Photo
* Camera: Video
* Camera: Switch between back and front camera
* Cellular: Carrier info, signal strength
* Cellular: Data connection
* Cellular: Incoming, outgoing calls
* Cellular: MMS in, out
* Cellular: PIN unlock
* Cellular: SMS in, out
* Cellular: Change audio routings
* Cellular: Voice in calls
* Cellular: Switch connection speed between 2G/3G/4G works for all SIMs
* Cellular: Enable/disable mobile data and flightmode works
* Cellular: Switch preferred SIM for calling and SMS - only for devices that support it
* Endurance: Battery lifetime > 24h from 100%
* Endurance: No reboot needed for 1 week
* GPU: Boot into UI
* GPU: Hardware video decoding
* Misc: Anbox patches applied to kernel
* Misc: AppArmor patches applied to kernel
* Misc: Battery percentage
* Misc: Offline charging (Power down, connect USB cable, device should not boot to UT)
* Misc: Online charging (Green charging symbol, percentage increase in stats etc)
* Misc: Recovery image builds and works
* Misc: Reset to factory defaults
* Misc: Date and time are correct after reboot (go to flight mode before)
* Misc: SD card detection and access - only for devices that support it
* Misc: Shutdown / Reboot
* Misc: Wireless charging - only for devices that support it
* (Network: NFC - disabled atm due to no middleware)
* WiFi: Driver loaded at startup
* WiFi: Enable/disable and flightmode works
* WiFi: Persistent MAC address between reboots
* WiFi: Hotspot can be configured, switched on and off, can serve data to clients
* Sensors: Automatic brightness
* Sensors: Fingerprint reader, register and use fingerprints (Halium 9.0 only)
* Sensors: GPS
* Sensors: Proximity
* Sensors: Rotation
* Sensors: Touchscreen
* Sound: Earphones detected, volume control
* Sound: Loudspeaker
* Sound: Microphone
* Sound: Loudspeaker volume control
* USB: MTP access
* (USB: ADB access - disabled atm due to no middleware)
* USB: External monitor - only for devices that support it
